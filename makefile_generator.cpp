#include "makefile_generator.hpp"

#include <algorithm>

#include <queue>


#include <iostream>


bool in(fs::path f, std::queue<fs::path> q){
	while(!q.empty()){
		if(q.front() == f) return true;
		q.pop();
	}
	return false;
}

bool in(fs::path f, std::vector<fs::path> q){
	for(const fs::path& p : q){
		if(p == f) return true;
	}
	return false;
}

bool in(std::string f, std::vector<std::string> q){
	for(const std::string& p : q){
		if(p == f) return true;
	}
	return false;
}


bool in(fs::path file, fs::path folder){
	if(!fs::exists(folder)) throw std::runtime_error("Error: folder " + folder.string() + " does not exist");
	if(!fs::is_directory(folder)) throw std::runtime_error("Error: " + folder.string() + " is not a folder");
	for(const fs::path& f : fs::recursive_directory_iterator(folder)){
		if(f.filename() == file.filename()){
			return true;
		}
	}
	return false;
}





//return true if string1 contains string2
bool is_included(std::string string1, std::string string2){
    for(size_t i = 0; i < string1.size(); i++){
        if(string1[i] == string2[0]){
            bool found = true;
            for(size_t j = 1; j < string2.size(); j++){
                if(string1[i + j] != string2[j]){
                    found = false;
                    break;
                }
            }
            if(found){
                return true;
            }
        }
    }
    return false;
}




bool is_custom_lib(fs::path header){ // ex: command.hpp
	fs::path c_path = GetEnvVar("C_INCLUDE_PATH")[0]; //the first one is the custom include path
	fs::path cxx_path = GetEnvVar("CPLUS_INCLUDE_PATH")[0]; //the first one is the custom include path

	//verify if the header is in one of the custom include paths
	if(in(header, c_path) || in(header, cxx_path)){
		return true;
	}
	return false;
}

std::string get_lib_from_header(fs::path header){
	fs::path storage_path = GetEnvVar("MAKELIB_STORAGE_PATH")[0]+"/dependencies.data";

	//read the file
	std::ifstream filestream(storage_path);
	std::string line;
	while (std::getline(filestream, line)){
		std::vector<std::string> line_split = Utility::split(line, '=');
		if(line_split.size() != 2) throw std::runtime_error("Error: invalid line in storage file: " + line);
		if(line_split[0] == header.string()){
			return line_split[1];
		}
	}
	return ""; //if the header is not in the file, it's only a custom header
}





bool is_source(fs::path f){
	return f.extension() == ".cpp" || f.extension() == ".c";
}
bool is_header(fs::path f){
	return f.extension() == ".hpp" || f.extension() == ".h";
}


std::vector<fs::path> get_includes(fs::path file){
    std::vector<fs::path> includes;
    std::ifstream filestream(file);
    std::string line;
    unsigned ln = 0; //line number
    while (std::getline(filestream, line)){
		if(!((line.size() == 0) || (line[0] != '#'))){ //skip line that are not #(something) (for efficiency)
			if(is_included(line, "#include") && line.find("#include") == 0){
				if(is_included(line, "<")){
					std::string include = line.substr(line.find("<") + 1);
					include = include.substr(0, include.find(">"));
					includes.push_back(include);
				}
				else if(is_included(line, "\"")){
					std::string include = line.substr(line.find("\"") + 1);
					include = include.substr(0, include.find("\""));
					includes.push_back(include);
				}
				else{
					throw std::runtime_error("Error: #include not followed by \" or < in file " + file.string() + ", on line " + std::to_string(ln));
				}
			}
		}
        ln++;
    }
    return includes;
}


//return the list of all the headers included in the file, recursively
std::vector<fs::path> get_recursive_includes(fs::path file, std::vector<fs::path>& system_libs, std::vector<fs::path> includes = {}){
	if(in(file, includes) || in(file, system_libs)) return includes; //if the file is already in the list, it has already been processed
	if(!fs::exists(file)){ //if the file does not exist, it is probably a system header
		if(!in(file, system_libs)){
			system_libs.push_back(file);
		}
		return includes;
	}
	if(is_header(file)) includes.push_back(file);
	std::vector<fs::path> new_includes = get_includes(file);
	for(const fs::path& include : new_includes){
		includes = get_recursive_includes(include, system_libs, includes);
	}
	
	return includes;
}


//return a vector of all headers required for the custom lib
std::vector<fs::path> get_required_for_custom_lib(fs::path header){
	if(!is_custom_lib(header)) throw std::runtime_error("Error: " + header.string() + " is not a custom lib");
	
	fs::path folder;
	if(header.extension() == ".h"){
		folder = GetEnvVar("C_INCLUDE_PATH")[0];
	}
	else if(header.extension() == ".hpp"){
		folder = GetEnvVar("CPLUS_INCLUDE_PATH")[0];
	}
	else{
		throw std::runtime_error("Error: " + header.string() + " is not a header");
	}

	fs::path file = folder/header;

	std::vector<fs::path> includes;
	std::queue<fs::path> to_process;
	to_process.push(file);

	while(!to_process.empty()){
		fs::path current = to_process.front();
		to_process.pop();

		std::vector<fs::path> includes_found = get_includes(current);
		for(const fs::path& include : includes_found){
			if(is_header(include)){
				if(!in(include, includes)){
					includes.push_back(include);
					to_process.push(include);
				}
			}
		}
	}
	return includes;
}


//return the list of all the libs included in the file, recursively
std::vector<std::string> get_recursive_lib_from_header(fs::path header){
	if(!is_custom_lib(header)) return {}; //if the header is not a custom header, it's not a lib
	std::vector<std::string> libs;
	libs.push_back(get_lib_from_header(header));
	
	std::vector<fs::path> system_libs;
	get_recursive_includes(header, system_libs);
	for(const fs::path& include : system_libs){
		libs.push_back(get_lib_from_header(include));
	}
	return libs;
}




Makefile::Makefile()
	: use_debug(false), is_cpp(false){
}

Makefile::Makefile(const Makefile& source)
	: final_targets(source.final_targets), use_debug(source.use_debug), is_cpp(source.is_cpp){
}

Makefile::Makefile(const fs::path& _final_target, bool _use_debug)
	: final_targets(), use_debug(_use_debug), is_cpp(false){
		if(!fs::exists(_final_target)){
			throw std::runtime_error("Error: file " + _final_target.string() + " does not exist");
		}
		if(!fs::is_regular_file(_final_target)){
			throw std::runtime_error("Error: " + _final_target.string() + " is not a file");
		}
		final_targets.push_back(_final_target);
		if(_final_target.extension() == ".cpp") is_cpp = true;
}

Makefile::Makefile(const std::vector<fs::path>& _final_targets, bool _use_debug)
	: final_targets(), use_debug(_use_debug), is_cpp(false){
	for(const fs::path& p: _final_targets){
		if(fs::exists(p)){
			if(fs::is_regular_file(p)){
				final_targets.push_back(p);
				if(p.extension() == ".cpp") is_cpp = true;
			}
			else{
				throw std::runtime_error("Error: '" + p.string() + "' is not a file");
			}
		}
		else{
			throw std::runtime_error("Error: file '" + p.string() + "' does not exist");
		}
	}
}


//when using command in makefile, use the makefile variables defined in Makefile::header
// $(CC) is the C compiler, for example

//write the compilation rule for the target
//dependencies only contains the headers files (.hpp or .h)
//the source file has the same name as the target, but with the .cpp or .c extension
//all headers have the .hpp  or .h extension
std::string write_compilation_rule(fs::path target, std::vector<fs::path> dependencies, bool use_debug){
	std::string prefix;

	fs::path source = target;
	source.replace_extension(".cpp");
	if(!fs::exists(source)) source = source.replace_extension(".c");
	if(!fs::exists(source)) throw std::runtime_error("Error: source file " + source.string() + " does not exist");
	
	if(source.extension() == ".cpp") {
		prefix = "$(CXX)";
		use_debug? prefix += " $(CXXDEBUGFLAGS)" : prefix += " $(CXXRELEASEFLAGS)";
	}
	else if(source.extension() == ".c"){
		prefix = "$(CC)";
		use_debug? prefix += " $(CDEBUGFLAGS)" : prefix += " $(CRELEASEFLAGS)";
	}
	else throw std::runtime_error("Error: source file " + source.string() + " is not a C or C++ file");

	std::string rule;

	rule = target.string() + ": " + source.string() + " ";
	for(const fs::path& dependency : dependencies){
		rule += dependency.string() + " ";
	}
	rule += "\n\t" + prefix + " -c " + source.string() + " -o " + target.string();

	return rule;
}

//write the linking rule for the target
//dependencies contains all the object files (.o) that the target depends on
//the target is an executable, and the dependencies are the object files
std::string write_linking_rule(fs::path target, std::vector<fs::path> dependencies, std::vector<std::string> required_libs, bool is_cpp){
	std::string rule;

	std::string cmd;
	if(is_cpp) cmd = "$(CXX)";
	else cmd = "$(CC)";

	rule = target.string() + ": ";
	for(const fs::path& dependency : dependencies){
		rule += dependency.string() + " ";
	}
	rule += "\n\t" + cmd + " -o " + target.string();
	for(const fs::path& dependency : dependencies){
		rule += " " + dependency.string();
	}
	//we only have the libname, we need to add the -l prefix
	for(const std::string& lib : required_libs){
		rule += " -l" + lib;
	}

	return rule;
}

//write the all rule
std::string write_all_rule(std::vector<fs::path> targets){
	std::string rule;

	rule = "all: ";
	for(const fs::path& target : targets){
		rule += target.string() + " ";
	}

	return rule;
}

//write the clean rule
std::string write_clean_rule(){
	std::string rule;

	rule = "clean:\n\t$(RM) *.o *.exe";

	return rule;
}

//write a update rule (clean and all)
std::string write_update_rule(){
	return "update: clean all";
}


fs::path exec2source(fs::path file){
	if(fs::exists(file.replace_extension(".cpp"))) return file;
	else if(fs::exists(file.replace_extension(".c"))) return file;
	else throw std::runtime_error("Error: source file " + file.string() + " does not exist");
}

//verify if the header file has a source file (with the same name, but with the .cpp or .c extension)
bool has_source(fs::path header){
	if(header.extension() == ".hpp"){
		return fs::exists(header.replace_extension(".cpp"));
	}
	else if(header.extension() == ".h"){
		return fs::exists(header.replace_extension(".c"));
	}
	else{
		throw std::runtime_error("Error: file " + header.string() + " is not a header file");
	}
}

//return the name of the source file corresponding to the header file
fs::path header2source(fs::path h){
	if(h.extension() == ".hpp"){
		return h.replace_extension(".cpp");
	}
	else if(h.extension() == ".h"){
		return h.replace_extension(".c");
	}
	else{
		throw std::runtime_error("Error: file " + h.string() + " is not a header file");
	}
}

//return the name of the object file corresponding to the source file
fs::path source2object(fs::path s){
	return s.replace_extension(".o");
}

fs::path object2source(fs::path o){
	return exec2source(o);
}

fs::path source2exec(fs::path s){
	return s.replace_extension(".exe");
}


void Makefile::print(std::ostream& os) const{
	//write the header
	for(const std::string& line : header){
		os << line << std::endl;
	}
	os << std::endl;

	std::queue<fs::path> object_queue; //object files waiting to be processed
	std::vector<fs::path> object_processed; //object files already processed
	std::queue<fs::path> executable_queue; //executable files waiting to be processed
	std::vector<fs::path> executable_processed; //executable files already processed

	std::vector<fs::path> targets; //usef for the "all" rule

	for(const fs::path& s : final_targets){
		fs::path exec = source2exec(s);
		executable_queue.push(exec);
		targets.push_back(exec);
	}



	//write the all rule
	// if(!main_file.empty()) targets.push_back(main_exec);
	// if(!test_file.empty()) targets.push_back(test_exec);
	os << write_all_rule(targets) << std::endl << std::endl;

	//write the update rule
	os << write_update_rule() << std::endl << std::endl;


	while(!executable_queue.empty()){
		fs::path executable = executable_queue.front();
		executable_queue.pop();

		//if the executable has already been processed, skip it
		if(in(executable, executable_processed)) continue;
		executable_processed.push_back(executable);

		//get the corresponding source file
		fs::path source = exec2source(executable);

		//get the list of all the headers included in the executable
		std::vector<fs::path> system_header;
		std::vector<fs::path> includes = get_recursive_includes(source, system_header);


		//system_header contain all the header files that are required for the eecutable, but aren't part of the project
		//maybe they are part of the standard library, or maybe they are part of a custom library

		std::vector<fs::path> required_objects; //list of all the object files (.o) that the executable depends on
		std::vector<std::string> required_custom_libs; //list of all the custom libraries that the executable depends on

		//for all the system headers, if they are a custom library, add the corresponding library to the list of dependencies
		for(auto i: system_header){
			if(is_custom_lib(i)){
				std::vector<fs::path> libs = get_required_for_custom_lib(i);
				libs.insert(libs.begin(), i);
				if(!libs.empty()){
					for(fs::path j: libs){
						std::string libname = get_lib_from_header(j);
						if(libname != "" && !in(libname, required_custom_libs)) required_custom_libs.push_back(libname);
					}
				}
			}
		}

		//add the corrsponding object file the the required objects (ex : main.exe need main.o)
		fs::path object = source2object(source);
		if(!in(object, object_processed)){
			object_queue.push(object);
		}
		if(!in(object, required_objects)) required_objects.push_back(object);

		//for all the headers, if they have a source file, add the corresponding object file to the queue, if it has not already been processed
		for(auto i: includes){
			if(has_source(i)){
				fs::path object = source2object(header2source(i));
				if(!in(object, object_processed)){
					object_queue.push(object);
				}
				if(!in(object, required_objects)) required_objects.push_back(object); //add the object file to the list of dependencies
			}
		}

		//write the linking rule for the executable
		os << write_linking_rule(executable, required_objects, required_custom_libs, is_cpp) << std::endl << std::endl;
	}
	
	//write the object files rules
	while(!object_queue.empty()){
		fs::path object = object_queue.front();
		object_queue.pop();

		//if the object has already been processed, skip it
		if(in(object, object_processed)) continue;
		object_processed.push_back(object);


		//get the corresponding source file
		fs::path source = object2source(object);

		//get the list of all the headers included in the source file
		std::vector<fs::path> system_header;
		std::vector<fs::path> includes = get_recursive_includes(source, system_header);
		//system_header contain all the header files that are required for the eecutable, but aren't part of the project
		//maybe they are part of the standard library, or maybe they are part of a custom library

		//we don't need to take care of the custom libraries here, because the object file will be linked with the executable, which will take care of the custom libraries


		//write the compilation rule for the object file
		os << write_compilation_rule(object, includes, use_debug) << std::endl << std::endl;

	}

	//write the clean rule
	os << write_clean_rule() << std::endl;

}

void Makefile::print(fs::path p) const{
	std::ofstream file(p);
	if(file.is_open()){
		print(file);
		file.close();
	}
	else{
		throw std::runtime_error("Error: could not open file " + p.string());
	}
}
